var geocoder;
var map;
var markers = Array();
var infos = Array();
function initialize() {
    
    geocoder = new google.maps.Geocoder();
   
    var myLatlng = new google.maps.LatLng(40.7143528,-74.0059731);

    var styledMapType = new google.maps.StyledMapType(
             [
               {elementType: 'geometry', stylers: [{color: '#ebe3cd'}]},
               {elementType: 'labels.text.fill', stylers: [{color: '#523735'}]},
               {elementType: 'labels.text.stroke', stylers: [{color: '#f5f1e6'}]},
               {
                 featureType: 'administrative',
                 elementType: 'geometry.stroke',
                 stylers: [{color: '#c9b2a6'}]
               },
               {
                 featureType: 'administrative.land_parcel',
                 elementType: 'geometry.stroke',
                 stylers: [{color: '#dcd2be'}]
               },
               {
                 featureType: 'administrative.land_parcel',
                 elementType: 'labels.text.fill',
                 stylers: [{color: '#ae9e90'}]
               },
               {
                 featureType: 'landscape.natural',
                 elementType: 'geometry',
                 stylers: [{color: '#dfd2ae'}]
               },
               {
                 featureType: 'poi',
                 elementType: 'geometry',
                 stylers: [{color: '#dfd2ae'}]
               },
               {
                 featureType: 'poi',
                 elementType: 'labels.text.fill',
                 stylers: [{color: '#93817c'}]
               },
               {
                 featureType: 'poi.park',
                 elementType: 'geometry.fill',
                 stylers: [{color: '#a5b076'}]
               },
               {
                 featureType: 'poi.park',
                 elementType: 'labels.text.fill',
                 stylers: [{color: '#447530'}]
               },
               {
                 featureType: 'road',
                 elementType: 'geometry',
                 stylers: [{color: '#f5f1e6'}]
               },
               {
                 featureType: 'road.arterial',
                 elementType: 'geometry',
                 stylers: [{color: '#fdfcf8'}]
               },
               {
                 featureType: 'road.highway',
                 elementType: 'geometry',
                 stylers: [{color: '#f8c967'}]
               },
               {
                 featureType: 'road.highway',
                 elementType: 'geometry.stroke',
                 stylers: [{color: '#e9bc62'}]
               },
               {
                 featureType: 'road.highway.controlled_access',
                 elementType: 'geometry',
                 stylers: [{color: '#e98d58'}]
               },
               {
                 featureType: 'road.highway.controlled_access',
                 elementType: 'geometry.stroke',
                 stylers: [{color: '#db8555'}]
               },
               {
                 featureType: 'road.local',
                 elementType: 'labels.text.fill',
                 stylers: [{color: '#806b63'}]
               },
               {
                 featureType: 'transit.line',
                 elementType: 'geometry',
                 stylers: [{color: '#dfd2ae'}]
               },
               {
                 featureType: 'transit.line',
                 elementType: 'labels.text.fill',
                 stylers: [{color: '#8f7d77'}]
               },
               {
                 featureType: 'transit.line',
                 elementType: 'labels.text.stroke',
                 stylers: [{color: '#ebe3cd'}]
               },
               {
                 featureType: 'transit.station',
                 elementType: 'geometry',
                 stylers: [{color: '#dfd2ae'}]
               },
               {
                 featureType: 'water',
                 elementType: 'geometry.fill',
                 stylers: [{color: '#b9d3c2'}]
               },
               {
                 featureType: 'water',
                 elementType: 'labels.text.fill',
                 stylers: [{color: '#92998d'}]
               }
             ],
             {name: 'Styled Map'});

    var myOptions = { 
        zoom: 14,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);
    map.mapTypes.set('styled_map', styledMapType);
    map.setMapTypeId('styled_map');

}

function clearOverlays() {
    if (markers) {
        for (i in markers) {
            markers[i].setMap(null);
        }
        markers = [];
        infos = [];
    }
}

function clearInfos() {
    if (infos) {
        for (i in infos) {
            if (infos[i].getMap()) {
                infos[i].close();
            }
        }
    }
}

function findAddress() {
    var address = document.getElementById("gmap_where").value;
    // script uses our 'geocoder' in order to find location by address name
    geocoder.geocode( { 'address': address}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) { 

            var addrLocation = results[0].geometry.location;
            map.setCenter(addrLocation);

            // storing current coordinates into hidden variables
            document.getElementById('lat').value = results[0].geometry.location.lat();
            document.getElementById('lng').value = results[0].geometry.location.lng();
            
            // adding new custom marker
            var addrMarker = new google.maps.Marker({
                position: addrLocation,
                map: map,
                title: results[0].formatted_address,
                icon: 'marker.png'
            });
        } else {
            alert('Geocode was not successful for the following reason: ' + status);
        }
    });
}

function findPlaces() {
    
    var type = document.getElementById('gmap_type').value;
    var radius = document.getElementById('gmap_radius').value;
    var keyword = document.getElementById('gmap_keyword').value;
    var lat = document.getElementById('lat').value;
    var lng = document.getElementById('lng').value;
    var cur_location = new google.maps.LatLng(lat, lng);
    
    var request = {
        location: cur_location,
        radius: radius,
        types: [type]
    };
    if (keyword) {
        request.keyword = [keyword];
    }
    // send request
    service = new google.maps.places.PlacesService(map);
    service.search(request, createMarkers);
}
// create markers (from 'findPlaces' function)
function createMarkers(results, status) {
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        
        clearOverlays();
        
        for (var i = 0; i < results.length; i++) {
            createMarker(results[i]);
        }
    } else if (status == google.maps.places.PlacesServiceStatus.ZERO_RESULTS) {
        alert('Sorry, nothing is found');
    }
}

function createMarker(obj) {
    
    var mark = new google.maps.Marker({
        position: obj.geometry.location,
        map: map,
        title: obj.name,
        icon: 'https://png.icons8.com/office/40/000000/map-pin.png'
    });
    markers.push(mark);
    // prepare info window
    var infowindow = new google.maps.InfoWindow({
        content: '<img src="' + obj.icon + '" /><font style="color:#000;">' + obj.name +
        '<br />Rating: ' + obj.rating + '<br />Vicinity: ' + obj.vicinity + '</font>'
    });
    // adding event handler to current marker
    google.maps.event.addListener(mark, 'click', function() {
        clearInfos();
        infowindow.open(map,mark);
    });
    infos.push(infowindow);
}
// initialization
google.maps.event.addDomListener(window, 'load', initialize);